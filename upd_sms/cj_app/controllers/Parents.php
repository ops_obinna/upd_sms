<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');



class Parents extends CI_Controller
{
    
    
    function __construct()
    {
        parent::__construct();
		$this->load->database();
        $this->load->library('session');
        /*cache control*/
        $this->output->set_header('Last-Modified: ' . gmdate("D, d M Y H:i:s") . ' GMT');
        $this->output->set_header('Cache-Control: no-store, no-cache, must-revalidate, post-check=0, pre-check=0');
        $this->output->set_header('Pragma: no-cache');
        $this->output->set_header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");
    }
    
    /***default functin, redirects to login page if no admin logged in yet***/
    public function index()
    {
        if ($this->session->userdata('parent_login') != 1)
            redirect(base_url() . 'index.php?login', 'refresh');
        if ($this->session->userdata('parent_login') == 1)
            redirect(base_url() . 'index.php?parents/dashboard', 'refresh');
    }
    
    /***ADMIN DASHBOARD***/
    function dashboard()
    {
        if ($this->session->userdata('parent_login') != 1)
            redirect(base_url(), 'refresh');
        $page_data['page_name']  = 'dashboard';
        $page_data['page_title'] = get_phrase('parent_dashboard');
        $this->load->view('backend/index', $page_data);
    }
    
    
    /****MANAGE TEACHERS*****/
    function teacher_list($param1 = '', $param2 = '', $param3 = '')
    {
        if ($this->session->userdata('parent_login') != 1)
            redirect(base_url(), 'refresh');
        if ($param1 == 'personal_profile') {
            $page_data['personal_profile']   = true;
            $page_data['current_teacher_id'] = $param2;
        }
        $page_data['teachers']   = $this->db->get('teacher')->result_array();
        $page_data['page_name']  = 'teacher';
        $page_data['page_title'] = get_phrase('manage_teacher');
        $this->load->view('backend/index', $page_data);
    }
    
    
    // ACADEMIC SYLLABUS
    function academic_syllabus($student_id = '')
    {
        if ($this->session->userdata('parent_login') != 1)
            redirect(base_url(), 'refresh');

        $page_data['page_name']  = 'academic_syllabus';
        $page_data['page_title'] = get_phrase('academic_syllabus');
        $page_data['student_id']   = $student_id;
        $this->load->view('backend/index', $page_data);
    }

    function download_academic_syllabus($academic_syllabus_code)
    {
        $file_name = $this->db->get_where('academic_syllabus', array(
            'academic_syllabus_code' => $academic_syllabus_code
        ))->row()->file_name;
        $this->load->helper('download');
        $data = file_get_contents("uploads/syllabus/" . $file_name);
        $name = $file_name;

        force_download($name, $data);
    }
    
    
    
    /****MANAGE SUBJECTS*****/
    function subject($param1 = '', $param2 = '')
    {
        if ($this->session->userdata('parent_login') != 1)
            redirect(base_url(), 'refresh');
        
        $parent_profile         = $this->db->get_where('parent', array(
            'parent_id' => $this->session->userdata('parent_id')
        ))->row();
        $parent_class_id        = $parent_profile->class_id;
        $page_data['subjects']   = $this->db->get_where('subject', array(
            'class_id' => $parent_class_id
        ))->result_array();
        $page_data['page_name']  = 'subject';
        $page_data['page_title'] = get_phrase('manage_subject');
        $this->load->view('backend/index', $page_data);
    }
    
    
    
    /****MANAGE EXAM MARKS*****/
    function marks($param1 = '')
    {
        if ($this->session->userdata('parent_login') != 1)
            redirect(base_url(), 'refresh');
        $page_data['student_id'] = $param1;
        $page_data['page_name']  = 'marks';
        $page_data['page_title'] = get_phrase('manage_marks');
        $this->load->view('backend/index', $page_data);
    }

    function student_marksheet_print_view($student_id , $exam_id) {
        if ($this->session->userdata('parent_login') != 1)
            redirect('login', 'refresh');
        $class_id     = $this->db->get_where('enroll' , array(
            'student_id' => $student_id , 'year' => $this->db->get_where('settings' , array('type'=>'running_year'))->row()->description
        ))->row()->class_id;
        $class_name   = $this->db->get_where('class' , array('class_id' => $class_id))->row()->name;

        $page_data['student_id'] =   $student_id;
        $page_data['class_id']   =   $class_id;
        $page_data['exam_id']    =   $exam_id;
        $this->load->view('backend/parent/student_marksheet_print_view', $page_data);
    }
    
    
    /**********MANAGING CLASS ROUTINE******************/
    function class_routine($param1 = '', $param2 = '', $param3 = '')
    {
        if ($this->session->userdata('parent_login') != 1)
            redirect(base_url(), 'refresh');
        
        $page_data['student_id'] = $param1;
        $page_data['page_name']  = 'class_routine';
        $page_data['page_title'] = get_phrase('manage_class_routine');
        $this->load->view('backend/index', $page_data);
    }

    function class_routine_print_view($class_id , $section_id)
    {
        if ($this->session->userdata('parent_login') != 1)
            redirect('login', 'refresh');
        $page_data['class_id']   =   $class_id;
        $page_data['section_id'] =   $section_id;
        $this->load->view('backend/teacher/class_routine_print_view' , $page_data);
    }
    
	 function school_calendar()
    {
        if ($this->session->userdata('parent_login') != 1)
            redirect('login', 'refresh');

		$query = $this->db->get_where('calendar', array('default_status' => 1))->result_array();
		foreach($query as $row){
		$query1 = $this->db->get_where('calendar_info', array('calendar_id' => $row['calendar_id']))->result_array();
		}
		 
        $page_data['calendar_view_info'] = $query1;
        $page_data['calendar_view'] = $query;
        $page_data['page_name'] = 'school_calendar';
        $page_data['page_title'] = get_phrase('school_calendar');
        $this->load->view('backend/index', $page_data);

    }

    function school_calendar_print_view($param1 = '', $param2 = '', $param3 = '')
    {
        if ($this->session->userdata('parent_login') != 1)
            redirect('login', 'refresh');

        $query = $this->db->get_where('calendar_info', array('calendar_id' => $param1));
        $query1 = $this->db->get_where('calendar', array('calendar_id' => $param1));
        if ($query->num_rows() < 1 || $query1->num_rows() < 1) {
            redirect(base_url() . 'index.php?parents/school_calendar', 'refresh');
        }
        $page_data['calendar_info'] = $query->result_array();
        $page_data['calendar'] = $query1->result_array();
        $this->load->view('backend/parent/school_calendar_print_view', $page_data);

    }

    
    /**********MANAGE LIBRARY / BOOKS********************/
    function book($param1 = '', $param2 = '', $param3 = '')
    {
        if ($this->session->userdata('parent_login') != 1)
            redirect('login', 'refresh');
        
        $page_data['books']      = $this->db->get('book')->result_array();
        $page_data['page_name']  = 'book';
        $page_data['page_title'] = get_phrase('manage_library_books');
        $this->load->view('backend/index', $page_data);
        
    }
    
    
    /**********WATCH NOTICEBOARD AND EVENT ********************/
    function noticeboard($param1 = '', $param2 = '', $param3 = '')
    {
        if ($this->session->userdata('parent_login') != 1)
            redirect('login', 'refresh');
        
        $page_data['notices']    = $this->db->get('noticeboard')->result_array();
        $page_data['page_name']  = 'noticeboard';
        $page_data['page_title'] = get_phrase('noticeboard');
        $this->load->view('backend/index', $page_data);
        
    }
    
    /**********MANAGE DOCUMENT / home work FOR A SPECIFIC CLASS or ALL*******************/
    function document($do = '', $document_id = '')
    {
        if ($this->session->userdata('parent_login') != 1)
            redirect('login', 'refresh');
        
        $page_data['page_name']  = 'manage_document';
        $page_data['page_title'] = get_phrase('manage_documents');
        $page_data['documents']  = $this->db->get('document')->result_array();
        $this->load->view('backend/index', $page_data);
    }
    
    /* private messaging */

    function message($param1 = 'message_home', $param2 = '', $param3 = '') {
        if ($this->session->userdata('parent_login') != 1)
            redirect(base_url(), 'refresh');

        if ($param1 == 'send_new') {
            $message_thread_code = $this->crud_model->send_new_private_message();
            $this->session->set_flashdata('flash_message', get_phrase('message_sent!'));
            redirect(base_url() . 'index.php?parents/message/message_read/' . $message_thread_code, 'refresh');
        }

        if ($param1 == 'send_reply') {
            $this->crud_model->send_reply_message($param2);  //$param2 = message_thread_code
            $this->session->set_flashdata('flash_message', get_phrase('message_sent!'));
            redirect(base_url() . 'index.php?parents/message/message_read/' . $param2, 'refresh');
        }

        if ($param1 == 'message_read') {
            $page_data['current_message_thread_code'] = $param2;  // $param2 = message_thread_code
            $this->crud_model->mark_thread_messages_read($param2);
        }
		if ($param1 == 'message_delete') {
			 if($param3 == substr(md5('sender'), 0, 6)){
			$data['sender_deleted'] = 1;
			}
			if($param3 == substr(md5('receiver'), 0, 8)){
				$data['receiver_deleted'] =	1;
			}
		if ($data['sender_deleted'] == 1 || $data['receiver_deleted'] == 1 )
		    $this->crud_model->delete_thread_messages($param2, $data);
			$this->session->set_flashdata('flash_message', get_phrase('message_deleted!'));
            redirect(base_url() . 'index.php?parents/message/' , 'refresh');
        }

        $page_data['message_inner_page_name']   = $param1;
        $page_data['page_name']                 = 'message';
        $page_data['page_title']                = get_phrase('private_messaging');
        $this->load->view('backend/index', $page_data);
    }
    
    /******MANAGE OWN PROFILE AND CHANGE PASSWORD***/
    function manage_profile($param1 = '', $param2 = '', $param3 = '')
    {
        if ($this->session->userdata('parent_login') != 1)
            redirect(base_url() . 'index.php?login', 'refresh');
        if ($param1 == 'update_profile_info') {
			
			/*Form Validation Rules 4 PROFILE UPDATE */
			
			$verify = array(
				array(
						'field'=>'name',
						'label'=>'Name',
						'rules'=>'required|min_length[3]|trim|max_length[30]|my_alpha',
						),
				
				array(
						'field'=>'email',
						'label'=>'Email',
						'rules'=>'trim|required|valid_email',
						)
			);
			
			$this->form_validation->set_rules($verify);
			$this->form_validation->set_message('is_unique', 'The %s you entered already exists');
			if($this->form_validation->run() == FALSE){
				$errors = validation_errors();
			if((isset($errors))&&($errors!='')){
			$page_data['errors'] = $errors;
			$page_data['page_name']  = 'manage_profile';
			$page_data['page_title'] = get_phrase('manage_profile');
			$page_data['edit_data']  = $this->db->get_where('parent', array(
            'parent_id' => $this->session->userdata('parent_id')
        ))->result_array();
        $this->load->view('backend/index', $page_data);
						}
			}
			 
			/* End Form Validation 4 PROFILE UPDATE */
			
			// If There are no errors => Proceed to Insert data in DB */
			
			else  { 
			
            $data['name']        = $this->input->post('name');
            $data['email']       = $this->input->post('email');
            
            $this->db->where('parent_id', $this->session->userdata('parent_id'));
            $this->db->update('parent', $data);
            $this->session->set_flashdata('flash_message', get_phrase('account_updated'));
            redirect(base_url() . 'index.php?parents/manage_profile/', 'refresh');
			}
        }
        if ($param1 == 'change_password') {
			
			/*Form Validation Rules 4 CHANGE PASSWORD */
			
			$verify = array(
				array(
						'field'=>'new_password',
						'label'=>'New Password',
						'rules'=>'required|trim|min_length[7]',
						),
				
				array(
						'field'=>'confirm_new_password',
						'label'=>'Confirm Password',
						'rules'=>'trim|required|matches[new_password]',
						)
			);
			
			$this->form_validation->set_rules($verify);
			$this->form_validation->set_message('is_unique', 'The %s you entered already exists');
			if($this->form_validation->run() == FALSE){
				$errors = validation_errors();
			if((isset($errors))&&($errors!='')){
			$page_data['errors'] = $errors;
			$page_data['page_name']  = 'manage_profile';
			$page_data['page_title'] = get_phrase('manage_profile');
			$page_data['edit_data']  = $this->db->get_where('parent', array(
            'parent_id' => $this->session->userdata('parent_id')
        ))->result_array();
        $this->load->view('backend/index', $page_data);
						}
			}
			 
			/* End Form Validation 4 PASSWORD CHANGE */
			
			// If There are no errors => Proceed to Insert data in DB */
			
			else  { 
			
            $data['password']             = $this->input->post('password');
            $data['new_password']         = $this->input->post('new_password');
            $data['confirm_new_password'] = $this->input->post('confirm_new_password');
            
            $current_password = $this->db->get_where('parent', array(
                'parent_id' => $this->session->userdata('parent_id')
            ))->row()->password;
            if (password_verify ($data['password'], $current_password )) {
                $this->db->where('parent_id', $this->session->userdata('parent_id'));
                $this->db->update('parent', array(
                    'password' => password_hash($data['new_password'], PASSWORD_DEFAULT)
                ));
                $this->session->set_flashdata('flash_message', get_phrase('password_updated'));
            } else {
                // Your Current paasword is incorrect
                
			echo '<script>alert(\'You have entered a wrong account passwod\');</script>';
            }
            redirect(base_url() . 'index.php?parents/manage_profile/', 'refresh');
			}
        }
		if($errors ==''){
        $page_data['page_name']  = 'manage_profile';
        $page_data['page_title'] = get_phrase('manage_profile');
        $page_data['edit_data']  = $this->db->get_where('parent', array(
            'parent_id' => $this->session->userdata('parent_id')
        ))->result_array();
        $this->load->view('backend/index', $page_data);
		}
    }
}
