<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Crud_model extends CI_Model
{

    function __construct()
    {
        parent::__construct();
    }

    function clear_cache()
    {
        $this->output->set_header('Cache-Control: no-store, no-cache, must-revalidate, post-check=0, pre-check=0');
        $this->output->set_header('Pragma: no-cache');
    }

    function get_type_name_by_id($type, $type_id = '', $field = 'name')
    {
        return $this->db->get_where($type, array($type . '_id' => $type_id))->row()->$field;
    }

    ////////PASSWORDS/////////////
    function get_pass($email)
    {
        $sql = "SELECT password FROM student WHERE email ='$email' UNION SELECT password FROM teacher WHERE email='$email'
         UNION SELECT password FROM parent WHERE email='$email' UNION SELECT password FROM admin WHERE email='$email'";
        $query = $this->db->query($sql);
        foreach ($query->result() as $row) {
            return $row->password;
        }
    }

//  <!--SET PASSWORD -->
    function set_pass($password)
    {
        if ($password) {
            return 'granted';
        } else {
            return 'denied';
        }

    }

    //////////Skin/////////////
    function set_skin($colour)
    {
        $skin['description'] = $colour;
        $this->db->set($skin);
        $this->db->where('type', 'skin_colour');
        $this->db->update('settings', $skin);
    }

    ////////STUDENT/////////////
    function get_students($class_id)
    {
        $query = $this->db->get_where('student', array('class_id' => $class_id));
        return $query->result_array();
    }

    function get_student_info($student_id)
    {
        $query = $this->db->get_where('student', array('student_id' => $student_id));
        return $query->result_array();
    }

    /////////TEACHER/////////////
    function get_teachers()
    {
        $query = $this->db->get('teacher');
        return $query->result_array();
    }

    function get_teacher_name($teacher_id)
    {
        $query = $this->db->get_where('teacher', array('teacher_id' => $teacher_id));
        $res = $query->result_array();
        foreach ($res as $row)
            return $row['name'];
    }

    function get_teacher_info($teacher_id)
    {
        $query = $this->db->get_where('teacher', array('teacher_id' => $teacher_id));
        return $query->result_array();
    }

    //////////SUBJECT/////////////
    function get_subjects()
    {
        $query = $this->db->get('subject');
        return $query->result_array();
    }

    function get_subject_info($subject_id)
    {
        $query = $this->db->get_where('subject', array('subject_id' => $subject_id));
        return $query->result_array();
    }

    function get_subjects_by_class($class_id)
    {
        $query = $this->db->get_where('subject', array('class_id' => $class_id));
        return $query->result_array();
    }

    function get_subject_name_by_id($subject_id)
    {
        $query = $this->db->get_where('subject', array('subject_id' => $subject_id))->row();
        return $query->name;
    }

    ////// student mark 4 each subject
    function get_subject_mark($exam_id, $class_id, $subject_id, $student_id)
    {
        $this->db->where('exam_id', $exam_id);
        $this->db->where('class_id', $class_id);
        $this->db->where('subject_id', $subject_id);
        $this->db->where('student_id', $student_id);
        $this->db->select('mark_obtained');
        $student_mark = $this->db->get('mark')->result_array();
        foreach ($student_mark as $row1) {
            return $row1;
        }
    }


///////////// SUBJECT POSITION 4 STUDENT
    function get_subject_position($exam_id, $class_id, $subject_id)
    {

        $this->db->where('exam_id', $exam_id);
        $this->db->where('class_id', $class_id);
        $this->db->where('subject_id', $subject_id);
        $this->db->select('mark_obtained');
        $this->db->order_by('mark_obtained DESC');

        return $this->db->get('mark')->result_array(); // Get ALL rows

    }
//////////////

    ////////////CLASS///////////
    function get_class_name($class_id)
    {
        $query = $this->db->get_where('class', array('class_id' => $class_id));
        $res = $query->result_array();
        foreach ($res as $row)
            return $row['name'];
    }

    function get_class_name_numeric($class_id)
    {
        $query = $this->db->get_where('class', array('class_id' => $class_id));
        $res = $query->result_array();
        foreach ($res as $row)
            return $row['name_numeric'];
    }

    function get_classes()
    {
        $query = $this->db->get('class');
        return $query->result_array();
    }

    function get_class_type($class_id)
    {
        $query = $this->db->get_where('class', array('class_id' => $class_id))->result_array();
        foreach ($query as $row)
            return $row['class_type'];
    }

    function get_class_info($class_id)
    {
        $query = $this->db->get_where('class', array('class_id' => $class_id));
        return $query->result_array();
    }

    //////////EXAMS/////////////
    function get_exams()
    {
        $query = $this->db->get_where('exam', array(
            'year' => $this->db->get_where('settings', array('type' => 'running_year'))->row()->description
        ));
        return $query->result_array();
    }

    function get_exam_info($exam_id)
    {
        $query = $this->db->get_where('exam', array('exam_id' => $exam_id));
        return $query->result_array();
    }

    function get_class_average($exam_id, $class_id, $running_year)
    {
        $this->db->select_avg('mark_obtained');
        $query = $this->db->get_where('mark', array('exam_id' => $exam_id, 'class_id' => $class_id, 'year' => $running_year));
        return $query->row()->mark_obtained;
    }

    function get_position($student_id, $exam_id, $class_id, $running_year)
    {
        $this->db->select_sum('mark_obtained');
        $query = $this->db->get_where('mark', array('student_id' => $student_id, 'exam_id' => $exam_id, 'class_id' => $class_id, 'year' => $running_year));
        return $query->row()->mark_obtained;
    }

    //////////GRADES/////////////
    function get_grades()
    {
        $query = $this->db->get('grade');
        return $query->result_array();
    }

    function get_grade_info($grade_id)
    {
        $query = $this->db->get_where('grade', array('grade_id' => $grade_id));
        return $query->result_array();
    }

    function get_obtained_marks($exam_id, $class_id, $subject_id, $student_id)
    {
        $marks = $this->db->get_where('mark', array(
            'subject_id' => $subject_id,
            'exam_id' => $exam_id,
            'class_id' => $class_id,
            'student_id' => $student_id))->result_array();

        foreach ($marks as $row) {
            echo $row['mark_obtained'];
        }
    }

    function get_highest_marks($exam_id, $class_id, $subject_id)
    {
        $this->db->where('exam_id', $exam_id);
        $this->db->where('class_id', $class_id);
        $this->db->where('subject_id', $subject_id);
        $this->db->select_max('mark_obtained');
        $highest_marks = $this->db->get('mark')->result_array();
        foreach ($highest_marks as $row) {
            echo $row['mark_obtained'];
        }
    }

    function get_grade($mark_obtained, $grade_type)
    {
        $query = $this->db->get_where('grade', array('grade_type' => $grade_type));
        $grades = $query->result_array();
        foreach ($grades as $row) {
            if ($mark_obtained >= $row['mark_from'] && $mark_obtained <= $row['mark_upto'])
                return $row;
        }
    }

    /** ADD ORDINAL SUFFIX TO POSITION **/
    function get_suffix($number)
    {
        $ends = array('th', 'st', 'nd', 'rd', 'th', 'th', 'th', 'th', 'th', 'th');
        if ((($number % 100) >= 11) && (($number % 100) <= 13))
            return $number . '<sup>th</sup>';
        else
            return $number . '<sup>' . $ends[$number % 10] . '</sup>';
    }

    /* GET TOTAL EXAM MARKS */
    function get_exam_total($exam_id, $class_id, $student_id)
    {
        $this->db->where('exam_id', $exam_id);
        $this->db->where('class_id', $class_id);
        $this->db->where('student_id', $student_id);
        $this->db->select_sum('mark_obtained');
        $exam_total = $this->db->get('mark')->result_array();
        foreach ($exam_total as $row) {
            return $row['mark_obtained'];
        }
    }
   /* ATTENDANCE*/
    function get_attendance($student_id, $class_id, $running_year){
        $check	=	array('student_id' => $student_id , 'status' => '1', 'class_id' => $class_id, 'year' => $running_year);
        $query = $this->db->get_where('attendance' , $check);
        return $query->num_rows();
    }

    function get_total_attendance($student_id, $class_id, $running_year){
       /* PRESENT*/
        $check	=	array('student_id' => $student_id , 'status' => '1', 'class_id' => $class_id, 'year' => $running_year);
        $query = $this->db->get_where('attendance' , $check);
        $present = $query->num_rows();
        /*ABSENT*/
        $check1	=	array('student_id' => $student_id , 'status' => '0', 'class_id' => $class_id, 'year' => $running_year);
        $query1 = $this->db->get_where('attendance' , $check1);
        $absent = $query1->num_rows();
        $total = $present + $absent;
        return $total;
    }

    function create_log($data)
    {
        $data['timestamp'] = strtotime(date('Y-m-d') . ' ' . date('H:i:s'));
        $data['ip'] = $_SERVER["REMOTE_ADDR"];
        $location = new SimpleXMLElement(file_get_contents('http://freegeoip.net/xml/' . $_SERVER["REMOTE_ADDR"]));
        $data['location'] = $location->City . ' , ' . $location->CountryName;
        $this->db->insert('log', $data);
    }

    function get_system_settings()
    {
        $query = $this->db->get('settings');
        return $query->result_array();
    }

    ////////BACKUP RESTORE/////////
    function create_backup($type)
    {
        $this->load->dbutil();


        $options = array(
            'format' => 'txt', // gzip, zip, txt
            'add_drop' => TRUE, // Whether to add DROP TABLE statements to backup file
            'add_insert' => TRUE, // Whether to add INSERT data to backup file
            'newline' => "\n"               // Newline character used in backup file
        );


        if ($type == 'all') {
            $tables = array('');
            $file_name = 'system_backup';
        } else {
            $tables = array('tables' => array($type));
            $file_name = 'backup_' . $type;
        }

        $backup = &$this->dbutil->backup(array_merge($options, $tables));


        $this->load->helper('download');
        force_download($file_name . '.sql', $backup);
    }

    /////////RESTORE TOTAL DB/ DB TABLE FROM UPLOADED BACKUP SQL FILE//////////
    function restore_backup()
    {
        move_uploaded_file($_FILES['userfile']['tmp_name'], 'uploads/backup.sql');
        $this->load->dbutil();


        $prefs = array(
            'filepath' => 'uploads/backup.sql',
            'delete_after_upload' => TRUE,
            'delimiter' => ';'
        );
        $restore = &$this->dbutil->restore($prefs);
        unlink($prefs['filepath']);
    }

    /////////DELETE DATA FROM TABLES///////////////
    function truncate($type)
    {
        if ($type == 'all') {
            $this->db->truncate('student');
            $this->db->truncate('mark');
            $this->db->truncate('teacher');
            $this->db->truncate('subject');
            $this->db->truncate('class');
            $this->db->truncate('exam');
            $this->db->truncate('grade');
        } else {
            $this->db->truncate($type);
        }
    }

    ////////IMAGE URL//////////
    function get_image_url($type = '', $id = '')
    {
        if (file_exists('uploads/' . $type . '_image/' . $id . '.jpg'))
            $image_url = base_url() . 'uploads/' . $type . '_image/' . $id . '.jpg';
        else
            $image_url = base_url() . 'uploads/user.jpg';

        return $image_url;
    }

    ////////STUDY MATERIAL//////////
    function save_study_material_info()
    {
        $data['timestamp'] = strtotime($this->input->post('timestamp'));
        $data['title'] = $this->input->post('title');
        $data['description'] = $this->input->post('description');
        $data['file_name'] = $_FILES["file_name"]["name"];
        $data['file_type'] = $this->input->post('file_type');
        $data['class_id'] = $this->input->post('class_id');

        $this->db->insert('document', $data);

        $document_id = $this->db->insert_id();
        move_uploaded_file($_FILES["file_name"]["tmp_name"], "uploads/document/" . $_FILES["file_name"]["name"]);
    }

    function select_study_material_info()
    {
        $this->db->order_by("timestamp", "desc");
        return $this->db->get('document')->result_array();
    }

    function select_study_material_info_for_student()
    {
        $student_id = $this->session->userdata('student_id');
        $class_id = $this->db->get_where('enroll', array(
            'student_id' => $student_id,
            'year' => $this->db->get_where('settings', array('type' => 'running_year'))->row()->description
        ))->row()->class_id;
        $this->db->order_by("timestamp", "desc");
        return $this->db->get_where('document', array('class_id' => $class_id))->result_array();
    }

    function update_study_material_info($document_id)
    {
        $data['timestamp'] = strtotime($this->input->post('timestamp'));
        $data['title'] = $this->input->post('title');
        $data['description'] = $this->input->post('description');
        $data['class_id'] = $this->input->post('class_id');

        $this->db->where('document_id', $document_id);
        $this->db->update('document', $data);
    }

    function delete_study_material_info($document_id)
    {
        $this->db->where('document_id', $document_id);
        $this->db->delete('document');
    }

    ////////private message//////
    function send_new_private_message()
    {
        $message = $this->input->post('message');
        $timestamp = strtotime(date("Y-m-d H:i:s"));

        $reciever = $this->input->post('reciever');
        $sender = $this->session->userdata('login_type') . '-' . $this->session->userdata('login_user_id');

        //check if the thread between those 2 users exists, if not create new thread
        $num1 = $this->db->get_where('message_thread', array('sender' => $sender, 'reciever' => $reciever))->num_rows();
        $num2 = $this->db->get_where('message_thread', array('sender' => $reciever, 'reciever' => $sender))->num_rows();

        if ($num1 == 0 && $num2 == 0) {
            $message_thread_code = substr(md5(rand(100000000, 20000000000)), 0, 15);
            $data_message_thread['message_thread_code'] = $message_thread_code;
            $data_message_thread['sender'] = $sender;
            $data_message_thread['reciever'] = $reciever;
            $this->db->insert('message_thread', $data_message_thread);
        }
        if ($num1 > 0)
            $message_thread_code = $this->db->get_where('message_thread', array('sender' => $sender, 'reciever' => $reciever))->row()->message_thread_code;
        if ($num2 > 0)
            $message_thread_code = $this->db->get_where('message_thread', array('sender' => $reciever, 'reciever' => $sender))->row()->message_thread_code;


        $data_message['message_thread_code'] = $message_thread_code;
        $data_message['message'] = $message;
        $data_message['sender'] = $sender;
        $data_message['timestamp'] = $timestamp;
        $this->db->insert('message', $data_message);

        // notify email to email reciever
        //$this->email_model->notify_email('new_message_notification', $this->db->insert_id());

        return $message_thread_code;
    }

    function send_reply_message($message_thread_code)
    {
        $message = $this->input->post('message');
        $timestamp = strtotime(date("Y-m-d H:i:s"));
        $sender = $this->session->userdata('login_type') . '-' . $this->session->userdata('login_user_id');


        $data_message['message_thread_code'] = $message_thread_code;
        $data_message['message'] = $message;
        $data_message['sender'] = $sender;
        $data_message['timestamp'] = $timestamp;
        $this->db->insert('message', $data_message);

        // notify email to email reciever
        //$this->email_model->notify_email('new_message_notification', $this->db->insert_id());
    }

    function mark_thread_messages_read($message_thread_code)
    {
        // mark read only the oponnent messages of this thread, not currently logged in user's sent messages
        $current_user = $this->session->userdata('login_type') . '-' . $this->session->userdata('login_user_id');
        $this->db->where('sender !=', $current_user);
        $this->db->where('message_thread_code', $message_thread_code);
        $this->db->update('message', array('read_status' => 1));
    }
	function delete_thread_messages($message_thread_code, $data){
		
	    $this->db->where('message_thread_code', $message_thread_code);
        $this->db->update('message', $data);
		$query = $this->db->get_where('message', array('sender_deleted' => 1, 'receiver_deleted' => 1 ));
			  if ($query->num_rows() > 0){
		$this->db->where(array('message_thread_code' => $message_thread_code, 'sender_deleted' => 1, 'receiver_deleted' => 1));
        $this->db->delete('message');
			  }
			  
		//Delete all Messages from main thread only if both sender and receiver have deleted their message
		 $sql = "`sender_deleted` = 1 AND `receiver_deleted` =0 OR `sender_deleted` =0 AND `receiver_deleted` = 1 OR `sender_deleted` =0 AND `receiver_deleted` =0";
		$this->db->where($sql);
		$query2 = $this->db->get('message');
			 if ($query2->num_rows() < 1){
		$this->db->where('message_thread_code', $message_thread_code);
		$this->db->delete('message_thread');
			}  
	}
	
	function user_deleted($sender, $current_user){
		if($sender == $current_user){
			return substr(md5('sender'), 0, 6);	
		}
		else {
			return substr(md5('receiver'), 0,8);
		}
	}

    function count_unread_message_of_thread($message_thread_code)
    {
        $unread_message_counter = 0;
        $current_user = $this->session->userdata('login_type') . '-' . $this->session->userdata('login_user_id');
        $messages = $this->db->get_where('message', array('message_thread_code' => $message_thread_code))->result_array();
        foreach ($messages as $row) {
            if ($row['sender'] != $current_user && $row['read_status'] == '0')
                $unread_message_counter++;
        }
        return $unread_message_counter;
    }
}
