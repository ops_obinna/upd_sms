<style>
td {
		text-align: center;
		
	}
</style>

<?php 
    $student_info = $this->crud_model->get_student_info($student_id);
    $exams         = $this->crud_model->get_exams();
	$class_type = $this->db->get_where('class' , array('class_id' => $class_id ))->row()->class_type;
    foreach ($student_info as $row1):
    foreach ($exams as $row2):
?>
    
<div class="row">
    <div class="col-md-12">
        <div class="panel panel-primary panel-shadow" data-collapsed="0">
            <div class="panel-heading">
                <div class="panel-title"><?php echo $row2['name'];?></div>
            </div>
            <div class="panel-body">
                   <table class="table table-bordered">
                       <thead>
                        <tr>
                            <td rowspan = "2">SUBJECT</td>
							<?php if($class_type == 'J'):?>
							<td style="text-align: center;" colspan="4">CA</td>
                            <td rowspan = "2">CA TOTAL (40)</td>
							<?php endif; ?>
							<?php if($class_type == 'S'):?>
							<td style="text-align: center;" colspan="3">CA</td>
                            <td rowspan = "2">CA TOTAL (30)</td>
							<?php endif; ?>
                            <td rowspan = "2">EXAM</td>
							<td rowspan = "2">FINAL MARKS</td>
                            <td rowspan = "2">GRADE</td>
							<td rowspan = "2">POSITION</td>
							<?php if($class_type == 'J'):?>
                            <td rowspan = "2">REMARKS</td>
							<?php endif; ?>
							<td rowspan = "2">SIGN</td>
                        </tr>
					   <tr>
						   <?php if($class_type == 'J'):?>
							   <td>T1</td>
							   <td>T2</td>
							   <td>T3</td>
							   <td>T4</td>
						   <?php endif; ?>
						   <?php if($class_type == 'S'):?>
							   <td>T1</td>
							   <td>T2</td>
							   <td>T3</td>
						   <?php endif; ?>
					   </tr>
                    </thead>
					
                    <tbody>
                        <?php 
                            $total_marks = 0;
                            $subjects = $this->db->get_where('subject' , array(
                                'class_id' => $class_id , 'year' => $running_year
                            ))->result_array();
                            foreach ($subjects as $row3):
                        ?>
                            <tr>
                                <td><?php echo $row3['name'];?></td>
								 <?php
                                        $obtained_mark_query = $this->db->get_where('mark' , array(
                                                    'subject_id' => $row3['subject_id'],
                                                        'exam_id' => $row2['exam_id'],
                                                            'class_id' => $class_id,
                                                                'student_id' => $student_id , 
                                                                    'year' => $running_year));
                                        if ( $obtained_mark_query->num_rows() > 0):
                                            $marks = $obtained_mark_query->result_array();
                                            foreach ($marks as $row4):
                                                
                                                $total_marks += $row4['mark_obtained'];
                                            
                                        
                                    ?>
									<!-- 1ST  CA -->
                                <td><?php echo $row4['t1']; ?></td>
								 <!-- 2ND  CA -->
								 <td><?php echo $row4['t2']; ?></td>
								 <!-- 3RD  CA -->
								  <td><?php echo $row4['t3']; ?></td>
								   <!-- TOTAL CA & 4TH TEST IF CLASS TYPE IS JUNIOR -->
								  <?php if($class_type  == 'J'):?>
								  <td><?php echo $row4['t4']; ?></td>
								  <?php endif; ?>
								   <!-- TOTAL CA -->
								  <td><?php echo $row4['tca']; ?></td>
								   <!-- EXAM -->
								  <td><?php echo $row4['exm']; ?></td>
								   <!-- FINAL MARKS -->
								  <td><?php echo $row4['mark_obtained'];?></td>
								  
								<?php endforeach;?>
								<?php endif;?>
								<!-- GRADE -->
                                <td>
                                    <?php
									if($class_type == 'J'){
										$grade_type = $class_type;
									}
									else if($class_type == 'S'){
										$grade_type = $class_type;
									}
                                        if($obtained_mark_query->num_rows() > 0) {
                                            if ($row4['mark_obtained'] >= 0 || $row4['mark_obtained'] != '') {
                                                $grade = $this->crud_model->get_grade($row4['mark_obtained'], $grade_type);
                                                echo $grade['name'];
                                               
                                            }
                                        }
                                    ?>
                                </td>
							<!-- POSITION START -->
	<?php

	// individual student mark 4 each subject in Class
	$subject_mark = $this->crud_model->get_subject_mark($row2['exam_id'], $class_id, $row3['subject_id'], $row1['student_id']);

	//**** Marks of All Student 4 each subject in Class ****//
	$subj_pos = $this->crud_model->get_subject_position($row2['exam_id'], $class_id, $row3['subject_id']);
	////////////////
	//**DETERMINE STUDENT POSITION IN PARTICULAR SUBJECT **/
	$array = array();

	foreach ($subj_pos as $row) {

		$subject_pos = $row['mark_obtained'];

		$array[] = $subject_pos;
		$s_position = array_combine(range(1, count($array)), array_values($array));
		if ($subject_mark['mark_obtained'] > 0) {
			$result = array_search($subject_mark['mark_obtained'], $s_position);
		}
	}
	if (!empty($result)  && $row4['mark_obtained'] > 0):
		$subject_position = $this->crud_model->get_suffix($result);
		?>
		<td><?php echo $subject_position; ?></td>

	<?php endif; ?>
	<?php if(empty($result) || $row4['mark_obtained'] < 1): ?>
		<td><?php echo "-"; ?></td>
	<?php endif; ?>

								<!-- POSITION END -->
								<!-- REMARK OR COMMENT -->
								<?php if ($class_type == 'J'):?>
                                <td>
                                    <?php 
									$grade_type = $class_type;
									if($obtained_mark_query->num_rows() > 0) {
                                            if ($row4['mark_obtained'] >= 0 || $row4['mark_obtained'] != '') {
                                                $grade = $this->crud_model->get_grade($row4['mark_obtained'], $grade_type);
                                                echo $grade['comment'];
                                               
                                            }
                                        }
                                    ?>
                                </td>
								<?php endif; ?>
								<td><!--SIGN HERE --> </td>
					
                            </tr>
                        <?php endforeach;?>
						
                    </tbody>
                   </table>

                   <hr />

                   <?php echo get_phrase('total_marks');?> : <?php echo $total_marks;?><br />
				   <?php echo get_phrase('class_average');?> : 
				   <?php $class_avg = $this->crud_model->get_class_average($row2['exam_id'], $class_id, $running_year);
				   echo round($class_avg, 1);				   
				   ?><br />
				   <?php echo get_phrase('position');?> :
				   <?php
				   $pos = array();
				   $all_students = $this->db->get_where('enroll' , array('class_id' => $class_id ))->result_array();
				   foreach ($all_students as $row6){
				   $position_all = $this->crud_model->get_position($row6['student_id'], $row2['exam_id'], $class_id, $running_year);
				   $pos[] = $position_all;
				   }
				   rsort($pos);
				   $pos_array = array_combine(range(1, count($pos)), array_values($pos));
				   $pos_result = array_search($total_marks, $pos_array);
				   $position = $this->crud_model->get_suffix($pos_result);
				   $no_of_students = $this->db->get_where('enroll', array('class_id' => $class_id, 'year' => $running_year))->num_rows();
				   echo $position.' out of '. $no_of_students ;
				   ?>
				  
                   <br>
                   <?php echo get_phrase('average');?> : 
                        <?php 
                        $this->db->where('class_id' , $class_id);
						$this->db->where('year' , $running_year);
						$this->db->from('subject');
						$number_of_subjects = $this->db->count_all_results();
						$average = $total_marks / $number_of_subjects;
						echo round($average, 1);
                        ?>

                    <br> <br>
                    <a href="<?php echo base_url();?>index.php?teacher/student_marksheet_print_view/<?php echo $student_id;?>/<?php echo $row2['exam_id'];?>" 
                        class="btn btn-primary" target="_blank">
                        <?php echo get_phrase('print_marksheet');?>
                    </a>
               

            </div>
        </div>  
    </div>
</div>
<?php
    endforeach;
        endforeach;
?>