<hr />
<div class="mail-env">

    <!-- Mail Body -->
    <div class="mail-body">

        <!-- message page body -->
        <?php include $message_inner_page_name . '.php'; ?>
    </div>

    <!-- Sidebar -->
    <div class="mail-sidebar" style="min-height: 800px;">

        <!-- compose new email button -->
        <div class="mail-sidebar-row hidden-xs">
            <a href="<?php echo base_url(); ?>index.php?parents/message/message_new" class="btn btn-success btn-icon btn-block">
                <?php echo get_phrase('new_message'); ?>
                <i class="entypo-pencil"></i>
            </a>
        </div>

        <!-- message user inbox list -->
        <ul class="mail-menu">

            <?php
            $current_user = $this->session->userdata('login_type') . '-' . $this->session->userdata('login_user_id');
			$messages = $this->db->get('message')->result_array();
			foreach ($messages as $row2){			
			$this->db->where(array('sender' => $current_user));
            $this->db->or_where('reciever' , $current_user);
            $message_threads = $this->db->get('message_thread')->result_array();
			}
			 foreach ($message_threads as $row):
			if ($current_user == $row2['sender']) {
				$q = $this->db->get_where('message', array('message_thread_code' => $row['message_thread_code'], 'sender_deleted' => 0 ));
			}
			else{
				$q = $this->db->get_where('message', array('message_thread_code' => $row['message_thread_code'], 'receiver_deleted' => 0 ));
			}
                // defining the user to show
                if ($row['sender'] == $current_user)
                    $user_to_show = explode('-', $row['reciever']);
				
                if ($row['reciever'] == $current_user)
                    $user_to_show = explode('-', $row['sender']);

                $user_to_show_type = $user_to_show[0];
                $user_to_show_id = $user_to_show[1];
                $unread_message_number = $this->crud_model->count_unread_message_of_thread($row['message_thread_code']);
                ?>
				
				<?php if($q->num_rows() > 0 ): ?>
				
                <li class="<?php if (isset($current_message_thread_code) && $current_message_thread_code == $row['message_thread_code']) echo 'active'; ?>">
                    <a href="<?php echo base_url(); ?>index.php?parents/message/message_read/<?php echo $row['message_thread_code']; ?>" style="padding:12px;">
                        <i class="entypo-dot"></i>

                        <?php echo $this->db->get_where($user_to_show_type, array($user_to_show_type . '_id' => $user_to_show_id))->row()->name; ?>

                        <span class="label label-info"><?php echo $user_to_show_type; ?></span>

                        <?php if ($unread_message_number > 0): ?>
                            <span class="badge badge-danger">
                                <?php echo $unread_message_number; ?>
                            </span>
                        <?php endif; ?>
                    </a>	
					<?php 
					$user = $this->crud_model->user_deleted($row['sender'], $current_user)
					?>
					<a href="#" style="padding:12px;" class="pull-right" onclick="confirm_modal('<?php echo base_url(); ?>index.php?parents/message/message_delete/<?php echo $row['message_thread_code']; ?>/<?=$user;?>');">
<i class="entypo-trash"></i></a>
                </li>
				 <?php endif; ?>
            <?php endforeach; ?>
			 
        </ul>

    </div>

</div>