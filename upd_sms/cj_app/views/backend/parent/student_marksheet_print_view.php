<?php
$class_name = $this->db->get_where('class', array('class_id' => $class_id))->row()->name;
$exam_name = $this->db->get_where('exam', array('exam_id' => $exam_id))->row()->name;
$system_name = $this->db->get_where('settings', array('type' => 'system_name'))->row()->description;
$running_year = $this->db->get_where('settings', array('type' => 'running_year'))->row()->description;
$class_type = $this->db->get_where('class', array('class_id' => $class_id))->row()->class_type;
$student_info = $this->crud_model->get_student_info($student_id);
$exams = $this->crud_model->get_exams();
$enroll_info = $this->db->get_where('enroll', array('class_id' => $class_id, 'student_id' => $student_id))->result_array();

foreach ($student_info as $row1):
	foreach ($exams as $row2):
		foreach ($enroll_info as $row3):
			?>

			<div class="container">
				<div id="print" xmlns:margin-top="http://www.w3.org/1999/xhtml">

					<script src="assets/js/jquery-1.11.0.min.js"></script>
					<link rel="stylesheet" type="text/css" href="assets/css/myprint.css" property=""/>
					<style type="text/css" scoped>
						td {
							padding: 5px;
							text-align: center;
							font-size: 11px;
						}
					</style>
					<!-- LOGO & SCHOOL DETAILS -->
					<div style="text-align: center;">
						<img src="uploads/logo.png" style="max-height : 60px;"><br/>
						<strong>FEDERAL UNIVERSITY OF TECHNOLOGY MINNA</strong><br/>
						<strong>MODEL SECONDARY SCHOOL</strong><br/>
						<strong>TERMINAL REPORT SHEET <?php
							if ($class_type == 'J') {
								echo '(' . strtoupper(get_phrase('junior_secondary')) . ')';
							} else if ($class_type == 'S') {
								echo '(' . strtoupper(get_phrase('senior_secondary')) . ')';
							}
							?>
						</strong>
						<br/>
					</div>
					<br>
					<!-- LOGO ENDS HERE-->
					<!--NAME , CLASS AND OTHER INFO -->
					<table style="width:100%; border-collapse:collapse; ">
						<tbody>
						<tr>
							<td><strong>NAME:</strong> <?php echo $row1['name']; ?></td>
							<td><strong>ADM NO:</strong> <?php echo $row3['roll']; ?></td>
							<td><strong>HOUSE</strong> <?php echo $row1['house']; ?></td>
							<td><strong>CLASS:</strong> <?php echo $class_name; ?></td>
						</tr>
						<tr>
							<td><strong>REPORT FOR</strong> <?php echo $row2['name']; ?></td>
							<td><strong>YEAR:</strong> <?php echo $row2['year']; ?></td>
							<td><strong>ATTENDANCE</strong>
								<?php
								$present = $this->crud_model->get_attendance($student_id, $class_id, $running_year);
								$total_attendance = $this->crud_model->get_total_attendance($student_id, $class_id, $running_year);
								echo $present .' DAYS OUT OF '. $total_attendance;
								?>
							</td>
							<!-- POSITION STARTS HERE -->
							<?php
							$pos = array();
							$all_students = $this->db->get_where('enroll', array('class_id' => $class_id))->result_array();
							foreach ($all_students as $row6) {
								$mark_all = $this->crud_model->get_position($row6['student_id'], $row2['exam_id'], $class_id, $running_year);
								$pos[] = $mark_all;
							}
							rsort($pos);
							$pos_array = array_combine(range(1, count($pos)), array_values($pos));
							$total_mark = $this->crud_model->get_position($student_id, $row2['exam_id'], $class_id, $running_year);
							$pos_result = array_search($total_mark, $pos_array);
							$position = $this->crud_model->get_suffix($pos_result);
							$no_of_students = $this->db->get_where('enroll', array('class_id' => $class_id, 'year' => $running_year))->num_rows();

							?>
							<td><strong>POSITION:</strong> <?php echo $position . ' OUT OF ' . $no_of_students; ?>
							</td>
						</tr>
						<!--POSITION ENDS HERE -->

						</tbody>
					</table>
					<!-- END INFO -->
					<table style="width:100%; border-collapse:collapse;border: 1px solid #ccc; margin-top: 10px;"
						   border="1">
						<thead>
						<tr>
							<td rowspan="2">SUBJECT</td>
							<?php if ($class_type == 'J'): ?>
								<td style="text-align: center;" colspan="4">CA</td>
								<td rowspan="2">CA TOTAL (40)</td>
							<?php endif; ?>
							<?php if ($class_type == 'S'): ?>
								<td style="text-align: center;" colspan="3">CA</td>
								<td rowspan="2">CA TOTAL (30)</td>
							<?php endif; ?>
							<td rowspan="2">EXAM</td>
							<td rowspan="2">FINAL MARKS</td>
							<td rowspan="2">GRADE</td>
							<td rowspan="2">POSITION</td>
							<?php if ($class_type == 'J'): ?>
								<td rowspan="2">REMARKS</td>
							<?php endif; ?>
							<td rowspan="2">SIGN</td>
						</tr>
						<tr>
							<?php if ($class_type == 'J'): ?>
								<td>T1</td>
								<td>T2</td>
								<td>T3</td>
								<td>T4</td>
							<?php endif; ?>
							<?php if ($class_type == 'S'): ?>
								<td>T1</td>
								<td>T2</td>
								<td>T3</td>
							<?php endif; ?>
						</tr>
						</thead>
						<tbody>
						<?php
						$total_marks = 0;
						$subjects = $this->db->get_where('subject', array(
							'class_id' => $class_id, 'year' => $running_year
						))->result_array();
						foreach ($subjects as $row3):
							?>
							<tr>
								<td><?php echo $row3['name']; ?></td>
								<?php
								$obtained_mark_query = $this->db->get_where('mark', array(
									'subject_id' => $row3['subject_id'],
									'exam_id' => $row2['exam_id'],
									'class_id' => $class_id,
									'student_id' => $student_id,
									'year' => $running_year));
								if ($obtained_mark_query->num_rows() > 0):
									$marks = $obtained_mark_query->result_array();
									foreach ($marks as $row4):

										$total_marks += $row4['mark_obtained'];


										?>
										<!-- 1ST  CA -->
										<td><?php echo $row4['t1']; ?></td>
										<!-- 2ND  CA -->
										<td><?php echo $row4['t2']; ?></td>
										<!-- 3RD  CA -->
										<td><?php echo $row4['t3']; ?></td>
										<!-- TOTAL CA & 4TH TEST IF CLASS TYPE IS JUNIOR -->
										<?php if ($class_type == 'J'): ?>
										<td><?php echo $row4['t4']; ?></td>
									<?php endif; ?>
										<!-- TOTAL CA -->
										<td><?php echo $row4['tca']; ?></td>
										<!-- EXAM -->
										<td><?php echo $row4['exm']; ?></td>
										<!-- FINAL MARKS -->
										<td><?php echo $row4['mark_obtained']; ?></td>

									<?php endforeach; ?>
								<?php endif; ?>
								<!-- GRADE -->
								<td>
									<?php
									if ($class_type == 'J') {
										$grade_type = $class_type;
									} else if ($class_type == 'S') {
										$grade_type = $class_type;
									}
									if ($obtained_mark_query->num_rows() > 0) {
										if ($row4['mark_obtained'] >= 0 || $row4['mark_obtained'] != '') {
											$grade = $this->crud_model->get_grade($row4['mark_obtained'], $grade_type);
											echo $grade['name'];

										}
									}
									?>
								</td>
								<!-- POSITION START -->
								<?php

								// individual student mark 4 each subject in Class
								$subject_mark = $this->crud_model->get_subject_mark($row2['exam_id'], $class_id, $row3['subject_id'], $row1['student_id']);

								//**** Marks of All Student 4 each subject in Class ****//
								$subj_pos = $this->crud_model->get_subject_position($row2['exam_id'], $class_id, $row3['subject_id']);
								////////////////
								//**DETERMINE STUDENT POSITION IN PARTICULAR SUBJECT **/
								$array = array();

								foreach ($subj_pos as $row) {

									$subject_pos = $row['mark_obtained'];

									$array[] = $subject_pos;
									$s_position = array_combine(range(1, count($array)), array_values($array));
									if ($subject_mark['mark_obtained'] > 0) {
										$result = array_search($subject_mark['mark_obtained'], $s_position);
									}
								}
								if (!empty($result) && $row4['mark_obtained'] > 0):
									$subject_position = $this->crud_model->get_suffix($result);
									?>
									<td><?php echo $subject_position; ?></td>

								<?php endif; ?>
								<?php if (empty($result) || $row4['mark_obtained'] < 1): ?>
									<td><?php echo "-"; ?></td>
								<?php endif; ?>

								<!-- POSITION END -->
								<!-- REMARK OR COMMENT -->
								<?php if ($class_type == 'J'): ?>
									<td>
										<?php
										$grade_type = $class_type;
										if ($obtained_mark_query->num_rows() > 0) {
											if ($row4['mark_obtained'] >= 0 || $row4['mark_obtained'] != '') {
												$grade = $this->crud_model->get_grade($row4['mark_obtained'], $grade_type);
												echo $grade['comment'];

											}
										}
										?>
									</td>
								<?php endif; ?>
								<td><!--SIGN HERE --> </td>

							</tr>
						<?php endforeach; ?>
						</tbody>
					</table>

					<!-- BOTTOM REPORT CARD STARTS-->
					<table style="width:100%;  margin-top: 5px;">
						<tbody>
						<tr>
							<!-- TOTAL MARKS -->
							<td>
								<!--AVG & CLASS AVG -->
								<strong> <?php echo get_phrase('affective_dev_avg_score'); ?>:</strong>
								<?php
								$this->db->where('class_id', $class_id);
								$this->db->where('year', $running_year);
								$this->db->from('subject');
								$number_of_subjects = $this->db->count_all_results();
								$average = $total_marks / $number_of_subjects;
								echo round($average, 1);
								?>
							</td>
							<td><strong><?php echo get_phrase('total_marks'); ?>:</strong><?php echo $total_marks; ?>
							</td>
							<td>
								<strong><?php echo get_phrase('broad_cum_score'); ?>:</strong>
								<?php $class_avg = $this->crud_model->get_class_average($row2['exam_id'], $class_id, $running_year);
								echo round($class_avg, 1);
								?>
							</td>
						</tr>
						</tbody>
					</table>
					<!--PSCHOMOTOR -->
					<table align="left"
						   style="width:35%; border-collapse:collapse;border: 1px solid #ccc; margin-top: 10px; margin-right: 10px;"
						   border="1">
						<thead>
						<tr>
							<td><strong>BEHAVIOUR</strong></td>
							<td><strong>GRADE</strong></td>
						</tr>
						</thead>
						<tbody>
						<tr>
							<?php
							$behaviour = $this->db->get_where(' report_card_behaviour', array('class_type' => $class_type))->result_array();
							foreach ($behaviour as $rowb):
							?>

							<td><?php echo $rowb['behaviour']; ?></td>
							<td><?php echo $rowb['grade']; ?></td>
						</tr>
						<?php endforeach; ?>
						</tbody>
					</table>

					<!-- SKILLS & GRADE -->
					<table align="left"
						   style="width:35%; margin-right: 10px; border-collapse:collapse;border: 1px solid #ccc;  margin-top: 10px;"
						   border="1">
						<thead>
						<tr>
							<td><strong>SKILLS</strong></td>
							<td><strong>GRADE</strong></td>
						</tr>
						</thead>
						<tbody>
						<?php
						$skills = $this->db->get_where(' report_card_skills', array('class_type' => $class_type))->result_array();
						foreach ($skills as $rows):
							?>
							<tr>
								<td><?php echo $rows['skills']; ?></td>
								<td><?php echo $rows['grade']; ?></td>
							</tr>
						<?php endforeach; ?>

						</tbody>
					</table>

					<!-- GRADING KEY -->
					<div style="margin-top: 10px; float: left">
						<strong>KEY TO THE GRADING</strong> <br/>

						<?php
						$query = $this->db->get_where('grade', array('grade_type' => $class_type))->result_array();
						foreach ($query as $rowg):
							?>
							<strong><?php echo $rowg['name']; ?></strong> => <?php echo $rowg['mark_from']; ?>  -  <?php echo $rowg['mark_upto']; ?>
							<br/>

						<?php endforeach; ?>
					</div>


					<!-- COMMENTS & SIGNATURE -->
					<div class="row">
						<table style=" border-collapse:collapse; width: 100%; margin-top:10px;">
							<tbody>
							<tr>
								<td><strong>FORM MASTER'S COMMENT:</strong>
									<?php
									for ($i = 0; $i <= 8; $i++) {
										echo '______';
									}
									?>
								</td>
							</tr>
							<tr>
								<td>
									<?php
									for ($i = 0; $i <= 9; $i++) {
										echo '______';
									}
									echo 'SIGN _______________';
									?>
								</td>
							</tr>
							<tr>
								<td><strong>PRINCIPAL'S COMMENT:</strong> <?php
									for ($i = 0; $i <= 8; $i++) {
										echo '______';
									}
									?></td>
							</tr>
							<tr>
								<td>
									<?php
									for ($i = 0; $i <= 9; $i++) {
										echo '______';
									}
									echo 'SIGN _______________';
									?>
								</td>
							</tr>
							<tr>
								<td>
									<strong>NEXT TERM BEGINS:</strong> <?php echo date('d-m-Y'); ?> <strong> FEES DUE
										BEFORE FIRST DAY OF TERM N </strong>_______
								</td>
							</tr>
							</tbody>
						</table>
					</div>

					<!--BOTTOM REPORT CARD ENDS -->


				</div>
			</div>

			<?php
		endforeach;
	endforeach;
endforeach;
?>