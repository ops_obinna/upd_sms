<style type="text/css">
    td {
        text-align: center;
    }
</style>
<hr/>
<div class="row">
    <div class="col-md-12">

        <!------CONTROL TABS START------>
        <ul class="nav nav-tabs bordered">

            <li class="active">
                <a href="#list" data-toggle="tab"><i class="entypo-menu"></i>
                    <?php echo get_phrase('calendar_list'); ?>
                </a></li>
            <li class="">
                <a href="#add" data-toggle="tab"><i class="entypo-plus-circled"></i>
                    <?php echo get_phrase('add_calendar'); ?>
                </a></li>


        </ul>
        <!------CONTROL TABS END------>
        <div class="tab-content">
            <br>
            <!-- CALENDAR LIST STARTS-->
            <div class="tab-pane box active" id="list" style="padding: 5px">
                <table class="table table-bordered datatable" id="table_export">
                    <thead>
                    <tr>
                        <th>
                            <div>#</div>
                        </th>
                        <th>
                            <div><?php echo get_phrase('calendar_name'); ?></div>
                        </th>
                        <th>
                            <div><?php echo get_phrase('term'); ?></div>
                        </th>
                        <th>
                            <div><?php echo get_phrase('year'); ?></div>
                        </th>
                        <th>
                            <div><?php echo get_phrase('status'); ?></div>
                        </th>
                        <th>
                            <div><?php echo get_phrase('options'); ?></div>
                        </th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php $count = 1;
                    foreach ($calendar as $row): ?>

                        <tr>
                            <td><?php echo $count++; ?></td>
                            <td><?php echo $row['name']; ?></td>
                            <td><?php echo $row['term']; ?></td>
                            <td><?php echo $row['year']; ?></td>
                            <?php if ($row['default_status'] == 1): ?>
                                <td><span class="label label-success">Default</span></td>
                            <?php endif; ?>
                            <?php if ($row['default_status'] == 0): ?>
                                <td><span class="label label-danger">Not Default</span></td>
                            <?php endif; ?>

                            <td>
                                <div class="btn-group">
                                    <button type="button" class="btn btn-default btn-sm dropdown-toggle"
                                            data-toggle="dropdown">
                                        Action <span class="caret"></span>
                                    </button>
                                    <ul class="dropdown-menu dropdown-default pull-right" role="menu">

                                        <!-- VIEW LINK -->
                                        <li>
                                            <a href="<?php echo base_url(); ?>index.php?admin/school_calendar_view/<?php echo $row['calendar_id']; ?>">
                                                <i class="entypo-window"></i>
                                                <?php echo get_phrase('view'); ?>
                                            </a>
                                        </li>
                                        <li class="divider"></li>
                                        <!-- MARK DEFAULT -->
                                        <li>
                                            <a href="<?php echo base_url(); ?>index.php?admin/school_calendar/mark_default/<?php echo $row['calendar_id']; ?>">
                                                <i class="entypo-star"></i>
                                                <?php echo get_phrase('mark_default'); ?>
                                            </a>
                                        </li>
                                        <li class="divider"></li>
                                        <!-- EDITING LINK -->
                                        <li>
                                            <a href="<?php echo base_url(); ?>index.php?admin/school_calendar_edit/<?php echo $row['calendar_id']; ?>">
                                                <i class="entypo-pencil"></i>
                                                <?php echo get_phrase('edit'); ?>
                                            </a>
                                        </li>
                                        <li class="divider"></li>

                                        <!-- DELETION LINK -->
                                        <li>
                                            <a href="#"
                                               onclick="confirm_modal('<?php echo base_url(); ?>index.php?admin/school_calendar/delete/<?php echo $row['calendar_id']; ?>');">
                                                <i class="entypo-trash"></i>
                                                <?php echo get_phrase('delete'); ?>
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                            </td>
                        </tr>
                    <?php endforeach; ?>

                    </tbody>
                </table>
            </div>
            <!-- CALENDAR LIST ENDS-->

            <div class="tab-pane box " id="add" style="padding: 5px">
                <div class="box-content">
                    <!-- CALENDAR CREATE STARTS-->

                    <hr/>
                    <div class="panel panel-primary panel-shadow" data-collapsed="0">
                        <div class="panel-heading">
                            <div class="panel-title">CALENDAR INFORMATION</div>
                        </div>
                        <div class="panel-body">
                            <div class="row">

                                <?php echo form_open(base_url() . 'index.php?admin/school_calendar/add', array('class' => 'form-groups-bordered validate', 'id' => 'myForm', 'name' => 'myForm')); ?>
                                <div class="row">
                                    <div class="col-md-3"></div>

                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label class="control-label"
                                                   style="margin-bottom: 5px;"><?php echo get_phrase('calendar_name'); ?></label>
                                            <input type="text" class="form-control" name="calendar_name"
                                                   data-validate="required"
                                                   data-message-required="<?php echo get_phrase('value_required'); ?>"
                                                   placeholder="E.g. FMS Calendar 1"/>
                                        </div>
                                    </div>

                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label for="term" class="control-label"
                                                   style="margin-bottom: 5px;"><?php echo get_phrase('term'); ?></label>
                                            <select name="term" id="term" class="form-control selectboxit"
                                                    data-validate="required"
                                                    data-message-required="Please select a term">

                                                <option value="1"><?php echo get_phrase('1st_term'); ?></option>
                                                <option value="2"><?php echo get_phrase('2nd_term'); ?></option>
                                                <option value="3"><?php echo get_phrase('3rd_term'); ?></option>
                                            </select>

                                        </div>
                                    </div>
                                    <div class="col-md-3"></div>
                                </div>

                                <!-- EVENTS INFO START -->
                                <hr/>
                                <div id="bulk_add_form">

                                    <div id="student_entry">
                                        <div class="row">

                                            <div class="col-md-offset-1 col-md-2">
                                                <div class="form-group">
                                                    <label class="control-label"
                                                           style="margin-bottom: 5px;"><?php echo get_phrase('week'); ?></label>
                                                    <input type="number" min="1" max="15" class="form-control"
                                                           name="week[]" data-validate="required"
                                                           data-message-required="<?php echo get_phrase('value_required'); ?>"
                                                           required/>
                                                    <div class="error"></div>
                                                </div>
                                            </div>

                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <label class="control-label"
                                                           style="margin-bottom: 5px;"><?php echo get_phrase('date'); ?></label>
                                                    <input type="text" required name="date[]"
                                                           class="form-control daterange daterange-inline"
                                                           data-format="MMMM D, YYYY"
                                                           placeholder="Start Date - End Date">

                                                </div>
                                            </div>


                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <label class="control-label"
                                                           style="margin-bottom: 5px;"><?php echo get_phrase('activities'); ?></label>
                                                    <textarea class="form-control autogrow" required name="activities[]"
                                                              id="field-ta"
                                                              placeholder="<?php echo get_phrase('activities'); ?>"></textarea>

                                                </div>
                                            </div>
                                            <div class="col-md-2">
                                                <div class="form-group">

                                                    <button type="button" class=" btn btn-default "
                                                            title="<?php echo get_phrase('remove'); ?>"
                                                            onclick="deleteParentElement(this)"
                                                            style="margin-top: 20px;">
                                                        <i class="entypo-trash" style="color: #696969;"></i>
                                                    </button>
                                                </div>
                                            </div>


                                        </div>
                                    </div>

                                    <div id="student_entry_append"></div>
                                    <br>
                                    <div class="row">
                                        <div style="text-align: center;">
                                            <button type="button" class="btn btn-default"
                                                    onclick="append_student_entry()">
                                                <i class="entypo-plus"></i> <?php echo get_phrase('add_a_row'); ?>
                                            </button>
                                        </div>
                                    </div>

                                    <br><br>


                                    <div class="row">

                                        <div style="text-align: center;">
                                            <button type="submit" id="validate"
                                                    class="btn btn-success"><?php echo get_phrase('save_calendar'); ?></button>
                                        </div>

                                    </div>
                                </div>
                                <?php echo form_close(); ?>


                            </div>
                        </div>

                        <!-- EVENTS INFO ENDS -->
                        <!-- CALENDAR CREATE ENDS -->

                    </div>
                </div>


            </div>


        </div>
    </div>

</div>

<script type="text/javascript">
    /*  $('#myForm input').on('change', function () {

     //Create array of input values
     var ar = $('#myForm input').map(function () {
     if ($(this).val() != '') return $(this).val()
     }).get();

     //Check if there are duplicates in array and show/hide msg
     ar.forEach(function (e) {
     (ar.indexOf(e) != ar.lastIndexOf(e)) ? $('.error').text('duplicate') : $('.error').text('')
     });

     });*/


    var blank_student_entry = '';
    $(document).ready(function () {
        //$('#bulk_add_form').hide();
        blank_student_entry = $("#student_entry").html();

        for ($i = 0; $i < 5; $i++) {
            $("#student_entry").append(blank_student_entry);
        }

    });

    function append_student_entry() {
        $("#student_entry_append").append(blank_student_entry);
		$("input.daterange").daterangepicker();
    }

    function deleteParentElement(n) {
        n.parentNode.parentNode.parentNode.parentNode.removeChild(n.parentNode.parentNode.parentNode);
    }


</script>