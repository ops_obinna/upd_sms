<style type="text/css">
    td {
        text-align: center;
    }
</style>
<hr />
            <div class="row">
                <div class="col-md-4"></div>
                <div class="col-md-4" style="text-align: center;">
                    <div class="tile-stats tile-gray">
                        <div class="icon"><i class="entypo-calendar"></i></div>
						
                        <h3 style="color: #696969;">ACTIVITIES OF EVENTS FOR</h3>
                       <?php
					   foreach ($calendar_view as $row3):
                        
                       if ($row3['term']==1){
                        $term = get_phrase('1st_term');
                        }
                        else if ($row3['term']==2){
                        $term = get_phrase('2nd_term');
                        }
                        else if ($row3['term']==3){
                        $term = get_phrase('3rd_term');
                        }?>
                        <h4 style="color: #696969;">
                            <?php echo $term.' '. $row3['year'];?>
                        </h4>
						<?php endforeach; ?>
                    </div>
                </div>
                <div class="col-md-4"></div>
            </div>

			
			<hr />

<div class="row">
	<div class="col-md-12">
		<table class="table table-bordered">
			<thead>
				<tr>
				<td><?php echo get_phrase('week');?> </td>
				<td><?php echo get_phrase('date');?></td>
				<td><?php echo get_phrase('activities');?></td>
				
				</tr>
			</thead>
			<tbody>
			<?php foreach($calendar_view_info as $row4):?>
				<tr>
					<td>
						<?php echo $row4['week'];?>
					</td>
					<td >
						<?php echo $row4['date'];?>
					</td>
					<td>
						<?php echo $row4['activity'];?>
					</td>
				
				</tr>

			<?php endforeach;?>

			</tbody>
		</table>
		<div style="text-align: center;">
			<a href="<?php echo base_url();?>index.php?admin/school_calendar_print_view/<?php echo $row3['calendar_id'];?>" 
				class="btn btn-primary" target="_blank">
				<?php echo get_phrase('print_calendar');?>
			</a>
		</div>
	</div>
</div>
