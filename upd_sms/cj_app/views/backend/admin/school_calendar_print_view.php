<?php
$system_name = $this->db->get_where('settings', array('type' => 'system_name'))->row()->description;
?>

<div id="print">

    <script src="assets/js/jquery-2.1.1.min.js"></script>
    <style type="text/css" scoped>
        td {
            padding: 5px;
            text-align: center;
        }

        h4 {
            color: #696969;
        }
    </style>

    <div style="text-align: center;">
        <img src="uploads/logo.png" alt="logo" style="max-height : 60px;"><br>
        <h3 style="font-weight: 100;"><?php echo $system_name; ?></h3>
        <h3>ACTIVITIES OF EVENTS FOR</h3>
        <?php
        foreach ($calendar as $row3):

            if ($row3['term'] == 1) {
                $term = get_phrase('1st_term');
            } else if ($row3['term'] == 2) {
                $term = get_phrase('2nd_term');
            } else if ($row3['term'] == 3) {
                $term = get_phrase('3rd_term');
            } ?>
            <h4>
                <?php echo $term . ' ' . $row3['year']; ?>
            </h4>
        <?php endforeach; ?>


    </div>

    <div class="col-md-4"></div>
    <div class="col-md-4">
        <table style="width:100%; border-collapse:collapse;border: 1px solid #ccc; margin-top: 10px;" border="1">
            <thead>
            <tr>
                <td><?php echo get_phrase('week'); ?></td>
                <td><?php echo get_phrase('date'); ?></td>
                <td><?php echo get_phrase('activities'); ?></td>
            </tr>
            </thead>
            <tbody>
            <?php foreach ($calendar_info as $row): ?>
                <tr>
                    <td><?php echo $row['week']; ?></td>
                    <td><?php echo $row['date']; ?></td>
                    <td><?php echo $row['activity']; ?></td>
                </tr>

            <?php endforeach; ?>

            </tbody>
        </table>
    </div>
    <div class="col-md-4"></div>
</div>


<script type="text/javascript">

    jQuery(document).ready(function ($) {
        var elem = $('#print');
        PrintElem(elem);
        Popup(data);

    });

    function PrintElem(elem) {
        Popup($(elem).html());
    }

    /**
     * @return {boolean}
     * @return {boolean}
     */
    function Popup(data) {
        var mywindow = window.open('', 'my div', 'height=400,width=600');
        mywindow.document.write('<html><head><title></title>');
        //mywindow.document.write('<link rel="stylesheet" href="assets/css/print.css" type="text/css" />');
        mywindow.document.write('</head><body >');
        //mywindow.document.write('<style>.print{border : 1px;}</style>');
        mywindow.document.write(data);
        mywindow.document.write('</body></html>');

        mywindow.document.close(); // necessary for IE >= 10
        mywindow.focus(); // necessary for IE >= 10

        mywindow.print();
        mywindow.close();

        return true;
    }
</script>