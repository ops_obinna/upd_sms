<hr />

<!-- CALENDAR EDIT STARTS-->


<div class="panel panel-primary panel-shadow" data-collapsed="0">
    <div class="panel-heading">
        <?php foreach($calendar as $row):
            if ($row['term']==1){
                $term = get_phrase('1st_term');
            }
            else if ($row['term']==2){
                $term = get_phrase('2nd_term');
            }
            else if ($row['term']==3){
                $term = get_phrase('3rd_term');
            }
            ?>
            <div class="panel-title">ACTIVITIES OF EVENTS FOR <?php echo $term.' '. $row['year']?></div>
        <?php endforeach; ?>
    </div>
    <div class="panel-body">
        <div class = "row">

            <?php echo form_open(base_url() . 'index.php?admin/school_calendar/do_update/'.$row['calendar_id'] , array('class' => 'form-groups-bordered validate', 'id'=>'myForm', 'name' =>'myForm'));?>
            <div class="row">

                <div class="col-md-3"></div>

                <div class="col-md-3">
                    <div class="form-group">
                        <label class="control-label"  style="margin-bottom: 5px;"><?php echo get_phrase('calendar_name');?></label>

                        <input type="text" class="form-control" name="calendar_name" data-validate="required" data-message-required="<?php echo get_phrase('value_required');?>" value="<?php echo $row['name']; ?>" />
                    </div>
                </div>

                <div class="col-md-3">
                    <div class="form-group">
                        <label class="control-label" style="margin-bottom: 5px;"><?php echo get_phrase('term');?></label>
                        <select name="term" id="term" class="form-control selectboxit" data-validate="required" data-message-required="Please select a term" >

                            <option value="1" <?php if ($row['term'] == 1){echo 'Selected';}?> ><?php echo get_phrase('1st_term');?></option>
                            <option value="2" <?php if ($row['term'] == 2){echo 'Selected';}?>><?php echo get_phrase('2nd_term');?> </option>
                            <option value="3" <?php if ($row['term'] == 3){echo 'Selected';}?>><?php echo get_phrase('3rd_term');?></option>
                        </select>

                    </div>
                </div>
                <div class="col-md-3"></div>
            </div>

            <!-- EVENTS INFO START -->
            <hr />
            <div id="bulk_add_form">
                <?php foreach($calendar_info as $row1): ?>
                    <div id="student_entry">
                        <div class="row">

                            <div class="col-md-offset-1 col-md-2">
                                <div class="form-group">
                                    <label  class="control-label"
                                            style="margin-bottom: 5px;"><?php echo get_phrase('week'); ?></label>
                                    <input type="number" min="1" max="15" class="form-control"
                                           name="week[]" value="<?php echo $row1['week']; ?>" data-validate="required"
                                           data-message-required="<?php echo get_phrase('value_required'); ?>"
                                           required/>
                                    <div class="error"></div>
                                </div>
                            </div>

                            <div class="col-md-3">
                                <div class="form-group">
                                    <label class="control-label"
                                           style="margin-bottom: 5px;"><?php echo get_phrase('date'); ?></label>
                                    <input type="text" required name="date[]"
                                           class="form-control date_range"
                                           data-format="MMMM D, YYYY"
                                           value="<?php echo $row1['date']; ?>">

                                </div>
                            </div>


                            <div class="col-md-3">
                                <div class="form-group">
                                    <label class="control-label"
                                           style="margin-bottom: 5px;"><?php echo get_phrase('activities'); ?></label>
                                                    <textarea class="form-control autogrow" required name="activities[]"
                                                              id="field-ta"
                                                              placeholder="<?php echo get_phrase('activities'); ?>"><?php echo $row1['activity']; ?></textarea>

                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group">

                                    <button type="button" class=" btn btn-default "
                                            title="<?php echo get_phrase('remove'); ?>"
                                            onclick="deleteParentElement(this)"
                                            style="margin-top: 20px;">
                                        <i class="entypo-trash" style="color: #696969;"></i>
                                    </button>
                                </div>
                            </div>


                        </div>
                    </div>
                <?php endforeach;?>
                <div id="student_entry_append"></div>
                <br>
                <div class="row">
                    <div style="text-align: center;">
                        <button type="button" class="btn btn-default" id="add_row"
                                >
                            <i class="entypo-plus"></i> <?php echo get_phrase('add_a_row'); ?>
                        </button>
                    </div>
                </div>

                <br><br>


                <div class="row">

                    <div style="text-align: center;">
                        <button type="submit" id="validate"
                                class="btn btn-success"><?php echo get_phrase('save_calendar'); ?></button>
                    </div>

                </div>
            </div>
            <?php echo form_close();?>








        </div>
    </div>

    <!-- EVENTS INFO ENDS -->
    <!-- CALENDAR CREATE ENDS -->

</div>
<script type="text/javascript">
    var blank_student_entry ='';
var date_pick ='';
    $(document).ready(function() {
        $('input.date_range').addClass("daterange daterange-inline");
        blank_student_entry = $('#student_entry').html();

        $('button#add_row').click(function(){
            append_student_entry();
        })
    });
    function append_student_entry()
    {
        $("#student_entry_append").append(blank_student_entry);
    }
    function deleteParentElement(n) {
        n.parentNode.parentNode.parentNode.parentNode.removeChild(n.parentNode.parentNode.parentNode);
    }


</script>