/* Insert Random Data in Table using MYSQL PROCEDURE 
tHIS UPDATES RANDOM INTEGERS BETWEEN 0 AND 10 INTO THE COLUMNS SPECIFIED
 */


DROP PROCEDURE IF EXISTS InsertRand;
DELIMITER $$
CREATE PROCEDURE InsertRand(IN NumRows INT, IN MinVal INT, IN MaxVal INT)
    BEGIN
        DECLARE i INT;
        SET i = 1;
		SET @st_id = 13;
		SET @cs_id = 2;
        START TRANSACTION;
        WHILE i <= NumRows DO
            UPDATE `mark` SET `t1` =  (MinVal + CEIL(RAND() * (MaxVal - MinVal))), `t2` = (MinVal + CEIL(RAND() * (MaxVal - MinVal))), `t3` = (MinVal + CEIL(RAND() * (MaxVal - MinVal))), `t4` = (MinVal + CEIL(RAND() * (MaxVal - MinVal)))  WHERE `mark`.`subject_id` = @st_id AND `mark`.`class_id` = @cs_id ;
            SET i = i + 1;
        END WHILE;
        COMMIT;
    END$$
DELIMITER ;

CALL InsertRand(4, 0, 10);